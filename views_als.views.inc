<?php

/**
 * @file
 * Defines the View Style Plugins for Views scroller module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_als_views_plugins() {
  return array(
    'style' => array(
      'slideshow' => array(
        'title' => t('Any List Scroller'),
        'help' => t('Display the results in a scrollable list.'),
        'handler' => 'ViewsPluginStyleViewsAls',
        'uses fields' => FALSE,
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'uses grouping' => FALSE,
        'uses row class' => TRUE,
        'type' => 'normal',
        'parent' => 'list',
        'theme' => 'views_view_als',
      ),
    ),
  );
}
