<?php

/**
 * @file
 * Template to display a views row in scrollable list.
 *
 * @ingroup views_templates
 */
?>
<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<div class="als-container" id=<?php print $scroller_id; ?>>
  <span class="als-prev"><img src="images/thin_left_arrow_333.png" alt="prev" title="previous" /></span>
    <?php print $wrapper_prefix; ?>
      <?php if (!empty($title)) : ?>
        <h3><?php print $title; ?></h3>
      <?php endif; ?>
      <?php print $list_type_prefix; ?>
        <?php foreach ($rows as $id => $row): ?>
          <li class="<?php print $classes_array[$id]; ?>"><?php print $row; ?></li>
        <?php endforeach; ?>
      <?php print $list_type_suffix; ?>
    <?php print $wrapper_suffix; ?>
  <span class="als-next"><img src="images/thin_right_arrow_333.png" alt="next" title="next" /></span>
</div>
