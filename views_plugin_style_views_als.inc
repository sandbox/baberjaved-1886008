<?php

/**
 * @file
 * Contains views_plugin_style_views_als.
 */

/**
 * Defines a style plugin that renders the full view as fieldset.
 *
 * @ingroup views_style_plugins
 */
class ViewsPluginStyleViewsAls extends views_plugin_style_list {

  /**
   * Overrides views_plugin_style::options_definition().
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['default_row_class'] = array('default' => FALSE, 'bool' => TRUE);
    $options['row_class_special'] = array('default' => FALSE, 'bool' => TRUE);
    $options['wrapper_class'] = array('default' => '');
    $options['orientation'] = array('default' => '');
    $options['visible_items'] = array('default' => '2');
    $options['scrolling_items'] = array('default' => '2');
    $options['circular'] = array('default' => '0');
    $options['autoscroll'] = array('default' => '0');

    return $options;
  }

  /**
   * Overrides views_plugin_style::options_form().
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $options = array(
      '' => t('- None -'),
      'horizontal' => t('Horizontal'),
      'vertical' => t('Vertical')
    );

    $form['orientation'] = array(
      '#type' => 'select',
      '#title' => t('Orientation'),
      '#options' => $options,
      '#default_value' => $this->options['orientation'],
      '#description' => t('Choose the type of orientation.'),
      '#weight' => -50,
    );

    $form['visible_items'] = array(
      '#type' => 'textfield',
      '#title' => t('Visible Items'),
      '#default_value' => $this->options['visible_items'],
      '#description' => t('Choose number of visible items in scroller.'),
      '#weight' => -49,
    );

    $form['scrolling_items'] = array(
      '#type' => 'textfield',
      '#title' => t('Scrolling Items'),
      '#default_value' => $this->options['scrolling_items'],
      '#description' => t('Choose number of scrollable items in scroller.'),
      '#weight' => -48,
    );

    $form['autoscroll'] = array(
      '#type' => 'checkbox',
      '#title' => t('Autoscroll'),
      '#return_value' => 'yes',
      '#default_value' => $this->options['autoscroll'],
      '#description' => t('Check if you want autoscroll.'),
      '#weight' => -47,
    );

    $form['circular'] = array(
      '#type' => 'checkbox',
      '#title' => t('Circular'),

      '#default_value' => $this->options['circular'],
      '#description' => t('Check if you want circular scroll.'),
      '#return_value' => 'yes',
      '#weight' => -46,
      '#states' => array(
        // Only show this field when the 'toggle_me' checkbox is enabled.
        'checked' => array(
          ':input[name="style_options[autoscroll]"]' => array('checked' => TRUE),
        )
      ),
    );

  }

}
