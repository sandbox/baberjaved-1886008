<?php

/**
 * @file
 * Views any_list_scroller style plugin code.
 */

/**
 * Implements hook_views_api().
 */
function views_als_views_api() {
  return array(
    'api' => 3.0,
  );
}


/**
 * Gets the path to the jQuery scroller library.
 *
 * @return bool
 *   The path to the scroller library js file, or FALSE if not found.
 */
function _views_als_library_path() {
  $scroller_path = FALSE;
  // Don't load javascript unless libraries module is present.
  if (module_exists('libraries')) {
    // Load jQuery als from the libraries path.
    $scroller_path = libraries_get_path('als');
    if (!empty($scroller_path)) {
      // Attempt to use minified version of jQuery als plugin.
      if (file_exists($scroller_path . '/jquery.als.min.js')) {
        $scroller_path .= '/jquery.als.min.js';
      }
      // Otherwise use non-minified version if available.
      elseif (file_exists($scroller_path . '/jquery.als.js')) {
        $scroller_path .= '/jquery.als.js';
      }
      else {
        $scroller_path = FALSE;
      }
    }
    else {
      $scroller_path = FALSE;
    }
  }
  return $scroller_path;
}

/**
 * Implements hook_preprocess_HOOK() for theme_views_view_fieldset().
 */
function template_preprocess_views_view_als(&$vars) {

  $module_path = drupal_get_path('module', 'views_als');
  if ($scroller_path = _views_als_library_path()) {
    drupal_add_js($scroller_path);
    drupal_add_js($module_path . '/js/views_als.js');
    drupal_add_css($module_path . '/css/views_als.css', 'file');
  }
  else {
    drupal_set_message(t('ALS Javascript Plugin missing or Libraries module not enabled.'), 'error');
  }

  $view = $vars['view'];
  $rows = $vars['rows'];
  $style = $view->style_plugin;
  $options = $style->options;

  $vars['view']->style_plugin->options['wrapper_class'] .= empty($options['wrapper_class']) ? 'als-viewport' : ' als-viewport';
  $vars['view']->style_plugin->options['class'] .= empty($options['class']) ? 'als-wrapper' : ' als-wrapper';
  $vars['view']->style_plugin->options['row_class'] .= empty($options['row_class']) ? 'als-item' : ' als-item';
  $html_id = $view->name . '-' . $view->current_display;

  static $scroller_id = array();
  if (isset($scroller_id[$html_id])) {
    $scroller_id[$html_id]++;
    $html_id .= "_" . $scroller_id[$html_id];
  }
  else {
    $scroller_id[$html_id] = 1;
  }
  $settings[$html_id] = $options;
  $defualts = array(
    'visible_items' => (int) $options['visible_items'],
    'scrolling_items' => (int) $options['scrolling_items'],
    'orientation' => $options['orientation'],
    'circular' => $options['circular'] == '0' ? 'no' : $options['circular'],
    'autoscroll' => $options['autoscroll'] == '0' ? 'no' : $options['circular'],
  );
  $settings[$html_id] = $defualts;
  drupal_add_js(array('views_als' => $settings), 'setting');
  $vars['scroller_id'] = $html_id;
  template_preprocess_views_view_list($vars);
}
