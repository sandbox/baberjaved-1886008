/**
 * @file
 * Views any_list_scroller Drupal Javascript Integration code
 */

(function($){

  Drupal.behaviors.viewsAls = {
   attach: function (context, settings) {
    var options = settings.views_als;
    $('.als-container', context).once('views-als', function(){
      console.log(options[this.id]['autoscroll']);
      console.log(options[this.id]['visible_items']);
      console.log(options[this.id]['scrolling_items']);
      console.log(options[this.id]['orientation']);
      console.log(options[this.id]['circular']);
      $(this).als(options[this.id]);
    });
 }
}})(jQuery);
